import React, { useEffect, useState } from "react";
import { DownOutlined, FileTextOutlined } from '@ant-design/icons';
import { Layout, Tree, theme } from 'antd';
import 'antd/dist/reset.css';
import { Link, useNavigate, useParams } from "react-router-dom";

const { Header, Content, Sider } = Layout;
const { DirectoryTree } = Tree;

let result = [];
let level = { result };
window.initValue?.pageFiles?.map(x => x.filename.replace(/^html\//i, '').replace(/\.html$/i, ''))
    .forEach((path) => {
        let key = "";
        path.split('/').reduce((r, title, i, a) => {
            const last = i < a.length - 1;
            key += title;
            if (!r[title]) {
                r[title] = { result: [] };
                const x = { title, key }
                if (last) {
                    x.children = r[title].result
                } else {
                    x.isLeaf = true;
                    x.icon = (<FileTextOutlined />)
                }
                r.result.push(x)
            }
            if (last) {
                key += '/';
            }
            return r[title];
        }, level)
    })

export function App() {
    const {
        token: { colorBgContainer },
    } = theme.useToken();

    const [iframeSrc, setIframeSrc] = useState();
    const params = useParams();
    const navigate = useNavigate();
    const treeData = result;

    useEffect(() => {
        const page = params['*'];
        if (page) {
            setIframeSrc(`page.html?p=/${page}.html`)
        } else {
            setIframeSrc(null);
        }
    }, [params])

    const onSelect = ([key], info) => {
        if (info.node.isLeaf) {
            navigate(`/page/${key}`)
        }
    }

    return (
        <Layout style={{ height: '100vh' }}>
            <div style={{width: '100%', height: '50px', display: 'flex', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', paddingLeft: '32px'}}>
                <h1 style={{marginBottom: '0'}}>Example Documentation</h1>
            </div>
            <Layout style={{height: 'calc(100% - 50px)'}}>
                <Sider
                    width={330}
                    style={{
                        background: colorBgContainer,
                        overflow: 'auto',
                    }}
                >
                    <DirectoryTree
                        showLine
                        multiple={false}
                        switcherIcon={<DownOutlined />}
                        defaultExpandedKeys={[params['*']]}
                        selectedKeys={[params['*']]}
                        onSelect={onSelect}
                        treeData={treeData}
                    />
                </Sider>
                <Layout
                    style={{
                        paddingLeft: '5px',
                    }}
                >
                    <Content
                        style={{
                            margin: 0,
                            height: '100vh',
                            background: colorBgContainer,
                        }}
                    >
                        {iframeSrc != null ?
                            <iframe name="iframe-content" title="content" src={iframeSrc} style={{ width: '100%', height: '100%', border: 'none', display: 'block' }}></iframe> : false
                        }
                    </Content>
                </Layout>
            </Layout>
        </Layout>
    )
}